let baseUrl;
let baseV2Url;
let env_url;

function getUrl(){
  if(process.env.NODE_ENV === 'development'){
    env_url = "https://galdaganystage.urup.com:4430/api/reports/"
  }else{
    // env_url = "https://reporting.urup.com:4430/api/reports/"
    env_url = "https://galdaganystage.urup.com:4430/api/reports/"
  }

  return env_url;
}

export function getVitalHeader (token, widgetId) {
  const url = getUrl() + "v2?request=apWidgetSummaryVitals&args=" +
              "{\"widget_id\":" + JSON.stringify(widgetId)  +
               ",\"platform\":" + "\"produk\"" +
               " ,\"token\":" + JSON.stringify(token) + "}";

  return fetch(url)
    .then(checkStatus)
    .then(parseJSON)
    .then(results => {
      return results.data.results[0];
    })
}


export function getChannelSummary (token, widgetId) {
  const url = getUrl() + "composite?request=apChannelsSummary&args=" +
              "{\"widget_id\":" + JSON.stringify(widgetId)  +
               ",\"platform\":" + "\"produk\"" +
                " ,\"token\":" + JSON.stringify(token) + "}";

  return fetch(url)
    .then(checkStatus)
    .then(parseJSON)
    .then(results => {
      return results.data.results;
    })
}

export function getJourneyDropOff (token, widgetId) {
    const url = getUrl() + "composite?request=apJourneyDropoff&args=" +
        "{\"widget_id\":" + JSON.stringify(widgetId)  +
        ",\"platform\":" + "\"produk\"" +
        " ,\"token\":" + JSON.stringify(token) + "}";

    return fetch(url)
        .then(checkStatus)
        .then(parseJSON)
        .then(results => {
            return results.data[0];
        })
}

export function getVitalGraph (token, widgetId) {
    const url = getUrl() + "composite?request=apDowHourlyActivity&args=" +
        "{\"widget_id\":" + JSON.stringify(widgetId)  +
        ",\"platform\":" + "\"produk\"" +
        " ,\"token\":" + JSON.stringify(token) + "}";

    return fetch(url)
        .then(checkStatus)
        .then(parseJSON)
        .then(results => {
            return results.data;
        })
}

export function getBlastReport (token, widgetId) {
    const url = getUrl() + "composite?request=apBlastLogs&args=" +
        "{\"widget_id\":" + JSON.stringify(widgetId)  +
        ",\"platform\":" + "\"produk\"" +
        " ,\"token\":" + JSON.stringify(token) + "}";

    return fetch(url)
        .then(checkStatus)
        .then(parseJSON)
        .then(results => {
            return results.data;
        })
}

export function getUserProfile (token, widgetId) {
    const url = getUrl() + "composite?request=apProfilesPerWidget&args=" +
        "{\"widget_id\":" + JSON.stringify(widgetId)  +
        ",\"platform\":" + "\"produk\"" +
        " ,\"token\":" + JSON.stringify(token) + "}";

    return fetch(url)
        .then(checkStatus)
        .then(parseJSON)
        .then(results => {
            return results.data;
        })
}

export function getDeviceReport (token, widgetId) {
    const url = getUrl() + "composite?request=apDeviceVisits&args=" +
        "{\"widget_id\":" + JSON.stringify(widgetId)  +
        ",\"platform\":" + "\"produk\"" +
        " ,\"token\":" + JSON.stringify(token) + "}";

    return fetch(url)
        .then(checkStatus)
        .then(parseJSON)
        .then(results => {
            return results.data;
        })
}


export function getFeedbackReport (token, widgetId) {
    const url = getUrl() + "composite?request=apFeedback&args=" +
        "{\"widget_id\":" + JSON.stringify(widgetId)  +
        ",\"platform\":" + "\"produk\"" +
        " ,\"token\":" + JSON.stringify(token) + "}";

    return fetch(url)
        .then(checkStatus)
        .then(parseJSON)
        .then(results => {
            return results.data;
        })
}

export function getConsumptionReport (token, widgetId) {
    const url = getUrl() + "composite?request=apConsumptionProfiles&args=" +
        "{\"widget_id\":" + JSON.stringify(widgetId)  +
        ",\"platform\":" + "\"produk\"" +
        " ,\"token\":" + JSON.stringify(token) + "}";

    return fetch(url)
        .then(checkStatus)
        .then(parseJSON)
        .then(results => {
            return results.data;
        })
}

function checkStatus (response){
  if(response.status >= 200 && response.status < 300) {
    return response;
  }else {
    const error = new Error(response.statusText);
    console.error(error);
    error.response = response;
    throw error;
  }
}

function parseJSON (response){
  return response.json();
}
