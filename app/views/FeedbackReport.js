import React from 'react';
import VitalHeader from '../components/stateless/Vitals/VitalHeader';
import FeedbackPollGraphTableAssembler from '../components/stateless/FeedbackReport/FeedbackPollGraphTableAssembler';
import FeedbackTextInput from '../components/stateless/FeedbackReport/FeedbackTextInput';
import FeedbackImageUploadTable from '../components/stateless/FeedbackReport/FeedbackImageUploadTable';
import {getFeedbackReport} from '../api';
import {isEmpty, has} from 'lodash';

class FeedbackReport extends React.Component {
    constructor() {
        super();
        this.state = {
            feedbackData: {}
        };
        this.getFeedbackData();
    }


    getFeedbackData() {
        getFeedbackReport(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({feedbackData: results});
            })
    }

    render() {
        if(!isEmpty(this.state.feedbackData) && has(this.state.feedbackData, 'profiles') && !this.state.feedbackData.profiles.length > 0){
            return (
                <div className="container-fluid">
                    <div className="wrapper-content">
                        <VitalHeader/>
                        <div className="ibox float-e-margins">
                            <div className="ibox-title bg-info">
                                <h3 className="text-uppercase">
                                </h3>
                            </div>
                            <div className="ibox-content text-capitalize font-bold text-center text-success">
                                <h1>No poll, text or upload gates configured for this journey</h1>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }

        return (
            <div className="container-fluid">
                <div className="wrapper-content">
                    <VitalHeader/>
                    {
                        this.state.feedbackData && has(this.state.feedbackData, 'summary') ?
                            this.state.feedbackData.summary.map((data, index) => {
                                if (data.type === 'poll') {
                                    return (
                                        <div key={index}>
                                            <FeedbackPollGraphTableAssembler
                                                poll={data}
                                                profiles={this.state.feedbackData["profiles"]}
                                            />
                                        </div>
                                    )
                                }
                                if (data.type === 'text') {
                                    return <FeedbackTextInput textInput={data}
                                                              profiles={this.state.feedbackData["profiles"]}/>
                                }
                                if (data.type === 'upload') {
                                    return <FeedbackImageUploadTable imageUpload={data}
                                                                     profiles={this.state.feedbackData["profiles"]}/>
                                }
                            })
                            :
                            <div>
                                <div className="sk-spinner sk-spinner-wordpress">
                                    <span className="sk-inner-circle"></span>
                                </div>
                            </div>
                    }

                </div>
            </div>
        )
    }
}

export default FeedbackReport;
