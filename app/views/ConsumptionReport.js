import React from 'react';
import VitalHeader from '../components/stateless/Vitals/VitalHeader';
import ConsumptionGraphAssembler from '../components/stateless/ConsumptionReport/ConsumptionGraphAssembler';
import {getConsumptionReport} from '../api';
import {isEmpty, has} from 'lodash';

class ConsumptionReport extends React.Component {
    constructor(){
        super();
        this.state = {
            consumptionData: {}
        };
        this.getConsumptionData();
    }

    getConsumptionData(){
        getConsumptionReport(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({consumptionData: results});
            })
    }
    render(){
        if(!isEmpty(this.state.consumptionData) && has(this.state.consumptionData, 'profiles') && !this.state.consumptionData.profiles.length > 0){
            return (
            <div className="container-fluid">
                <div className="wrapper-content">
                    <VitalHeader/>
                    <div className="ibox float-e-margins">
                        <div className="ibox-title bg-info">
                            <h3 className="text-uppercase">
                            </h3>
                        </div>
                        <div className="ibox-content text-capitalize font-bold text-center text-success">
                            <h1>Consumption information is currently unavailable</h1>
                        </div>
                    </div>
                </div>
            </div>
            )
        }
        return (
                <div className="container-fluid">
                    <div className="wrapper-content">
                        <VitalHeader/>
                        <ConsumptionGraphAssembler data={this.state.consumptionData}/>
                    </div>
                </div>
        )
    }
}

export default ConsumptionReport;
