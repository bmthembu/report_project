import React, {Component} from 'react';

import VitalHeader from '../components/stateless/Vitals/VitalHeader';
import SmsBlast from '../components/stateless/Blast/SmsBlast';
import { getBlastReport } from '../api';

class Main extends Component {
    constructor(){
        super();
        this.state = {
            blastData: {}
        };
        this.getBlastData();
    }
    getBlastData(){
        getBlastReport(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({blastData: results});
            })
    }
    render() {
        return (
            <div className="container-fluid">
                <div className="wrapper-content">
                    <VitalHeader/>
                    <SmsBlast blastData={this.state.blastData}/>
                    <br/>
                </div>
            </div>
        )
    }
}
export default Main 
