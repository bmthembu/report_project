import React, { Component } from 'react';
import ChannelOverview from '../components/stateless/Vitals/ChannelOverview';
import VitalGraphs from '../components/graphs/Vital/GraphsComposer';
import JourneyDropOff from '../components/stateless/Vitals/JourneyDropOff';
import VitalHeader from '../components/stateless/Vitals/VitalHeader';

class Main extends Component {
    constructor(props) {
        super(props);
        window.localStorage.setItem("token",this.props.params.token);
        window.localStorage.setItem("widgetId",this.props.params.widgetId);
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="wrapper-content">
                    <VitalHeader/>
                    <div className="clearfix"></div>
                    <ChannelOverview/>
                    <VitalGraphs/>
                    <JourneyDropOff/>

                </div>
            </div>
        )
    }
}

export default Main
