import React from 'react';
import VitalHeader from '../components/stateless/Vitals/VitalHeader';
import DeviceTable from '../components/stateless/DeviceReport/DeviceTable';
import DeviceGraphsComposer from '../components/graphs/Device/DeviceGraphsComposer';
import { getDeviceReport } from '../api';

class DeviceReport extends React.Component {
    constructor(){
        super();
        this.state = {
            deviceData: {}
        };
        this.getDeviceData();
    }
    getDeviceData(){
        getDeviceReport(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({deviceData: results});
            })
    }
    render(){
        return (
                <div className="container-fluid">
                    <div className="wrapper-content">
                        <VitalHeader/>
                        <DeviceGraphsComposer data={this.state.deviceData}/>
                        <DeviceTable data={this.state.deviceData}/>
                    </div>
                </div>
        )
    }
}

export default DeviceReport;
