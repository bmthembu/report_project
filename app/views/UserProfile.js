import React from 'react';
import VitalHeader from '../components/stateless/Vitals/VitalHeader';
import UserDetails from '../components/stateless/userProfile/UserDetails';
import { getUserProfile } from '../api';
import {isEmpty, has} from 'lodash';

class UserProfile extends React.Component {
    constructor(){
        super();
        this.state = {
            userProfileData: {}
        };
        this.getUserProfileData();
    }
    getUserProfileData(){
        getUserProfile(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({userProfileData: results.profiles});
            })
    }
    render() {
        if(!isEmpty(this.state.userProfileData) && has(this.state.userProfileData, 'profiles') && !this.state.userProfileData.profiles.length > 0){
            return (
                <div className="container-fluid">
                    <div className="wrapper-content">
                        <VitalHeader/>
                        <div className="text-capitalize font-bold text-center text-success">
                            <h1>User Profile information is currently unavailable</h1>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="container-fluid">
                <div className="wrapper-content">
                    <VitalHeader/>
                    <UserDetails userProfile={this.state.userProfileData}/>
                </div>
            </div>
        )
    }
}


export default UserProfile;