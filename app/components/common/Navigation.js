import React, {Component} from 'react';
import {Dropdown} from 'react-bootstrap';
import {Link, Location} from 'react-router';

class Navigation extends Component {

    componentDidMount() {
        const {menu} = this.refs;
        $(menu).metisMenu();
    }

    activeRoute(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "active" : "";
    }

    secondLevelActive(routeName) {
        return this.props.location.pathname.indexOf(routeName) > -1 ? "nav nav-second-level collapse in" : "nav nav-second-level collapse";
    }

    render() {
        return (
            <nav className="navbar-default navbar-static-side" role="navigation">
                <ul className="nav metismenu" id="side-menu" ref="menu">
                    <li className="nav-header">
                        <div className="logo-element">
                            URUP REPORTING
                        </div>
                    </li>
                    <li className={this.activeRoute("/vital")}>
                        <Link to="/vital"><i className="fa fa-heartbeat fa-fw"></i> <span
                            className="nav-label">Vitals</span></Link>
                    </li>
                    <li className={this.activeRoute("/blast")}>
                        <Link to="/blast"><i className="fa fa-code-fork fa-fw"></i> <span className="nav-label">Blast Report</span></Link>
                    </li>
                    <li className={this.activeRoute("/userProfile")}>
                        <Link to="/userProfile"><i className="fa fa-female fa-fw"></i> <span className="nav-label">User Profile</span></Link>
                    </li>
                    <li className={this.activeRoute("/feedback")}>
                        <Link to="/feedback"><i className="fa fa-microphone fa-fw"></i> <span className="nav-label">Feedback Report</span></Link>
                    </li>
                    <li className={this.activeRoute("/consumption")}>
                    <Link to="/consumption"><i className="fa fa-gamepad fa-fw"></i> <span className="nav-label">Consumption</span></Link>
                    </li>
                    <li className={this.activeRoute("/device")}>
                        <Link to="/device"><i className="fa fa-map-marker fa-fw"></i> <span className="nav-label">Device Report</span></Link>
                    </li>

                    {/*<li className={this.activeRoute("/shuffle")}>*/}
                    {/*<Link to="/"><i className="fa fa-gamepad fa-fw"></i> <span className="nav-label">Shuffle Up</span></Link>*/}
                    {/*</li>*/}
                    {/*<li className={this.activeRoute("/puzzle")}>*/}
                    {/*<Link to="/"><i className="fa fa-gamepad fa-fw"></i> <span className="nav-label">Puzzle Up</span></Link>*/}
                    {/*</li>*/}
                    {/*<li className={this.activeRoute("/activity")}>*/}
                    {/*<Link to="/"><i className="fa fa-bar-chart fa-fw"></i> <span className="nav-label">Activity</span></Link>*/}
                    {/*</li>*/}
                </ul>

            </nav>
        )
    }
}

export default Navigation
