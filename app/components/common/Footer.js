import React from 'react';

class Footer extends React.Component {
    render() {
        return (
            <div className="footer">
                <div className="pull-right">
                    Reporting <strong>MADE</strong> Easy.
                </div>
                <div>
                    <strong>Copyright</strong> URUP &copy; 2018
                </div>
            </div>
        )
    }
}

export default Footer