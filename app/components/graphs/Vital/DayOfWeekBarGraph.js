import React from 'react';
import {Bar} from 'react-chartjs-2';
import 'chartjs-plugin-datalabels';

export default (props) => {
    function renderData(data) {
        const weekDays =  data.map(function(day) {
            return day.day_name;
        });

        const visitors = data.map(function(v) {
            return v.visitors;
        });
        const visitorsDataSet = {
            label: 'Visitors',
            data: visitors,
            backgroundColor: ['#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d',
                '#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d',
                '#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d',
                '#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d','#00a99d'],
            hoverBackgroundColor: ['#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572',
                '#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572',
                '#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572',
                '#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572','#64E572'],
        };
        const users = data.map(function(u){
            return u.users;
        });
        const userDataSet = {
            label: 'Users',
            data: users,
            backgroundColor: ['#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6',
                '#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6',
                '#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6',
                '#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6',
                '#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6',
                '#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6','#1c84c6'],
            hoverBackgroundColor: ['#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed',
                '#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed',
                '#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed',
                '#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed','#57e8ed'],
        };
        return {
            labels: weekDays,
            datasets: [
                visitorsDataSet,
                userDataSet
            ]
        }
    }

    return (

        <div className="bg-white">
            <Bar
                data={renderData(props.data)}
                options={
                {
                    title: {
                        display: false,
                    },
                    legend: {
                        display: true,
                        position: "bottom"
                    },
                    plugins: {
                    datalabels: {
                        display: true,
                        align: 'top',
                        anchor: 'top'
                    }
                }
                }}
            />
        </div>

    )
}
