import React from 'react';
import {Doughnut} from 'react-chartjs-2';

export default (props) => {
    function renderData(data) {
        return {
            datasets: [{
                data: [data.after_hours.users, data.lunch.users, data.work_hours.users],
                backgroundColor: [
                    '#00a99d',
                    '#36A2EB',
                    '#1c84c6'
                ],
                hoverBackgroundColor: [
                    '#64E572',
                    '#36A2EB',
                    '#57e8ed'
                ]
            }],
            labels: [
                'After Hours',
                'Lunch',
                'Work Hour'
            ]
        }
    }

    return (

        <div className="bg-white">
            <Doughnut
                data={renderData(props.data)}
                options={{
                    title: {
                        display: false,
                    },
                    legend: {
                        display: true,
                        position: "bottom"
                    }
                }}
            />
        </div>

    )
}
