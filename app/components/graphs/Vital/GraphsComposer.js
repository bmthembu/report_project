import React from 'react';
import BarChart from './DayOfWeekBarGraph';
import PieActivityGraph from './ActivityReportGraph';
import HourlyGraph from './HourGraph';
import { getVitalGraph } from '../../../api';
import { has } from 'lodash';

class GraphComposer extends React.Component {
    constructor(){
        super();
        this.state = {
            graphResults: {},
            open: true
        };
        this.getGraphResults();
    }
    toggle() {
        this.setState({
            open: !this.state.open
        });
    }
    getGraphResults(){
        getVitalGraph(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({graphResults: results});
            })
    }
    render() {
        return (
            <div>
                <div className="col-sm-6 m-t-lg">
                    <div className="bg-info">
                        <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                            <span className="fa-color-new">Visitors</span> &amp; <span
                            className="fa-color-notice">Users </span>
                            Per Day Of The Week
                            <a className="collapsed collapse-link pull-right" onClick={this.toggle.bind(this)} role="button">
                                <i className="fa fa-chevron-up fa-color-white" style={{color: "#ffffff"}}></i>
                            </a>
                        </h3>

                    </div>
                    <div className={"collapse" + (this.state.open ? ' in' : '')}>
                    {
                        has(this.state.graphResults, 'day_of_the_week_activity') && this.state.graphResults.day_of_the_week_activity.length > 0 ?
                            <BarChart data={this.state.graphResults.day_of_the_week_activity}/>
                        :
                            <div>No information to generate the graph</div>

                    }
                    </div>
                </div>

                <div className="col-sm-6 m-t-lg">
                    <div className="bg-info">
                        <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                            24-Hour Activity Report
                            <a className="collapsed collapse-link pull-right" onClick={this.toggle.bind(this)} role="button">
                                <i className="fa fa-chevron-up fa-color-white" style={{color: "#ffffff"}}></i>
                            </a>
                        </h3>
                    </div>
                    <div className={"collapse" + (this.state.open ? ' in' : '')}>
                    {
                        has(this.state.graphResults, 'day_period_activity') ?
                            <PieActivityGraph data={this.state.graphResults.day_period_activity}/>
                            :
                            <div>No information to generate the graph</div>

                    }
                    </div>
                </div>
                <div className="clearfix"></div>
                <div className="col-sm-12 m-t-lg">
                    <div className="bg-info">
                        <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                            <span className="fa-color-new">Visitors</span> &amp; <span
                            className="fa-color-notice">Users </span>
                            Per Hour of the Day
                            <a className="collapsed collapse-link pull-right" onClick={this.toggle.bind(this)} role="button">
                                <i className="fa fa-chevron-up fa-color-white" style={{color: "#ffffff"}}></i>
                            </a>
                        </h3>
                    </div>
                    <div className={"collapse" + (this.state.open ? ' in' : '')}>
                    {
                        has(this.state.graphResults, 'hourly_activity') && this.state.graphResults.hourly_activity.length > 0 ?
                            <HourlyGraph data={this.state.graphResults.hourly_activity}/>
                            :
                            <div>No information to generate the graph</div>

                    }
                </div>
                </div>
            </div>
        )
    }
}

export default GraphComposer;
