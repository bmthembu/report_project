import React from 'react';
import {Pie} from 'react-chartjs-2';

export default (props) => {
    function renderData(data) {
        const device_pct = data.map((device) => {
            return device.visitors_pct;
        });
        const device_name = data.map((device) => {
            return device.form_factor;
        });

        return {

            datasets: [{
                data: device_pct,
                backgroundColor: [

                    '#64E572',
                    '#36A2EB',
                    '#00a99d',
                    '#57e8ed',
                    '#ff4f00',
                    '#1c3a53',
                    '#231f20',
                    '#1c84c6',
                    '#36A2EB',
                    '#1c3f95',
                    '#e06064',
                    '#4e6b81',
                    '#009acd',
                    '#ffc125',
                    '#1e90ff',
                    '#00a99d',
                    '#36A2EB',
                    '#1c84c6',
                    '#64E572',
                    '#36A2EB',
                    '#57e8ed',
                    '#ff4f00',
                    '#1c3a53',
                    '#231f20',
                    '#1c3f95',
                    '#e06064',
                    '#4e6b81',
                    '#009acd'

                ]
            }],
            labels: device_name
        }
    }

    return (

        <div className="bg-white">
            <Pie
                data={renderData(props.data)}
                height={180}
                options={{
                    title: {
                        display: false,
                    },
                    legend: {
                        display: true,
                        position: "bottom"
                    }
                }}
            />
        </div>

    )
}
