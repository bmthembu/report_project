import React from 'react';
import VisitsByDeviceChart from './VisitsByDeviceBarGraph';
import DiviceBreakDownGraph from './DeviceBreakdownReportGraph';
import TopTenGraph from './TopDeviceGraph';
import {has} from 'lodash';

class DeviceGraphsComposer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <div className="col-sm-6 m-t-lg">
                    <div className="col-sm-12">
                        <div className="bg-info">
                            <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                                <span className="fa-color-new">Device form factor breakdown</span>
                            </h3>
                        </div>
                        {
                            has(this.props.data, 'devices') && this.props.data.devices.length > 0 ?
                                <DiviceBreakDownGraph data={this.props.data.devices}/> :
                                <div>
                                    No information to generate the graph
                                </div>
                        }
                    </div>
                        <div className="col-sm-12 m-t-lg">
                            <div className="bg-info">
                                <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                                    <span className="fa-color-new">Visits By Device Type</span>
                                </h3>
                            </div>
                            {
                                has(this.props.data, 'devices') && this.props.data.devices.length > 0 ?
                                    <VisitsByDeviceChart data={this.props.data.devices}/>
                                    :
                                    <div>
                                        No information to generate the graph
                                    </div>
                            }
                        </div>
                </div>
                <div className="col-sm-6 m-t-lg">
                    <div className="bg-info">
                        <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                            Top 10 Devices by Visits
                        </h3>
                    </div>
                    {
                        has(this.props.data, 'top_ten') ?
                            <TopTenGraph data={this.props.data.top_ten}/>
                            :
                            <div>
                                No information to generate the graph
                            </div>
                    }
                </div>
            </div>
        )
    }
}

export default DeviceGraphsComposer;
