import React from 'react';
import {Bar} from 'react-chartjs-2';

export default (props) => {
    function renderData(data) {
        const device_name =  data.map(function(device) {
            return device.form_factor;
        });

        const visitors = data.map(function(v) {
            return v.visitors;
        });
        const visitorsDataSet = {
            label: 'Visits by Device Type',
            data: visitors,
            backgroundColor: ['#00b2ee','#64E572','#00bfff','#83b3d8','#3399ff','#0099cc','#0000ee',
                '#0f303a','#5f4b8b','#ffa90d','#263997','#509ce4','#085298','#f5f65d','#4c5f1c','#205dff',
                '#ffc220','#1279d1','#1a3253','#670000','#0f303a','#1c3a53','#7dc67b','#411350','#2d3751',
                '#db4227','#422306'],
            hoverBackgroundColor: [
                '#00a99d',
                '#36A2EB',
                '#1c84c6',
                '#64E572',
                '#36A2EB',
                '#57e8ed',
                '#ff4f00',
                '#1c3a53',
                '#231f20',
                '#1c3f95',
                '#e06064',
                '#4e6b81',
                '#ffc125',
                '#1e90ff',
                '#009acd'
            ],
        };

        return {
            labels: device_name,
            datasets: [ visitorsDataSet ]
        }
    }

    return (

        <div className="bg-white">
            <Bar
                data={renderData(props.data)}
                height={180}
                options={{
                    title: {
                        display: true,
                    },
                    legend: {
                        display: false,
                        position: "top"
                    }
                }}
            />
        </div>

    )
}
