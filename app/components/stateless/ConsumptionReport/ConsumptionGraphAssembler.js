import React from 'react';
import ConsumptionGraph from './ConsumptionGraph';
import VideoGraph from './VideoDoughNatGraph';
import ConsumptionTable from './ConsumptionTable';
import {has} from 'lodash';

class ConsumptionGraphAssembler extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <div>
                <div className="col-lg-12">
                    {
                        this.props.data && has(this.props.data, 'summary') ?
                            this.props.data.summary.map((data, index) => {
                                if (data.type === 'download') {
                                    if(this.props.download > 0){
                                        return (
                                            <div key={index}>
                                                <ConsumptionGraph data={data.chart_data}/>
                                                <ConsumptionTable overallSeq={data.overall_seq}
                                                                  profiles={this.props.data["profiles"]}/>
                                            </div>
                                        )
                                    }

                                }
                                if (data.type === 'content' || data.type === 'video') {
                                    return (
                                        <div>
                                            <VideoGraph data={data.chart_data} type={data.type}/>
                                            <ConsumptionTable overallSeq={data.overall_seq}
                                                              profiles={this.props.data["profiles"]}/>
                                        </div>
                                    )
                                }
                            })
                            :
                        <div>
                            <div className="sk-spinner sk-spinner-wordpress">
                                <span className="sk-inner-circle"></span>
                            </div>
                        </div>
                    }
                </div>
            </div>
        )
    }
}

export default ConsumptionGraphAssembler;

