import React from 'react';
import {HorizontalBar} from 'react-chartjs-2';
import {flatten} from 'lodash';

export default (props) => {
   function getChartData(data){
       let keys=[];
       data.map(function(v) {
           keys.push(Object.keys(v));
       });


       let values=[];
       data.map(function(v) {
           values.push(Object.values(v));
       });

        return {
            datasets: [{
                data: flatten(values),
                backgroundColor: [
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#FF9655','#FFF263','#6AF9C4','#8bbc21','#00a99d','#2f7ed8',
                    '#DDDF00','#24CBE5','#64E572','#FF9655','#FFF263','#6AF9C4',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#FF9655','#FFF263','#6AF9C4','#8bbc21','#00a99d','#2f7ed8',
                    '#DDDF00','#24CBE5','#64E572','#FF9655','#FFF263','#6AF9C4',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572',
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572'
                ],
                hoverBackgroundColor: [
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                ]
            }],
            labels: flatten(keys),
        }
    }
    return (
        <div>
            <div className="col-lg-12 bg-info">
                <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                    Download Report | [Gate instruction]
                </h3>
            </div>

            <div className="bg-white">
                    <HorizontalBar
                        height={70}
                        data={getChartData(props.data)}
                        options={{
                            title: {
                                display: false,
                            },
                            legend: {
                                display: false,
                                position: "top"
                            },
                            plugins: {
                                datalabels: {
                                    display: true,
                                    align: 'end',
                                    anchor: 'end'
                                }
                            }
                        }}
                    />

            </div>
        </div>
    )

}

