import React from 'react';
import {has} from 'lodash';


class ConsumptionTable extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        let profiles = [];
        this.props.profiles.filter((data) => {
                data.consumption.map((innerData) => {
                   if(innerData.seq === this.props.overallSeq){
                       profiles.push(data);
                   }
                });
        });

        let tableNames =profiles.map((profile) =>{
            return (
                <tr className="text-center">
                    <td>{profile.date}</td>
                    <td>{profile.full_name}</td>
                    <td><a href={"tel:" + profile.phone} className="fa-color-new">{profile.phone}</a></td>
                    <td><a href={"mailto:" + profile.email} className="fa-color-new">{profile.email}</a></td>
                    <td><i className="fa fa-check fa-fw fa-color-info"></i><span style={{display:"none"}}>
                        {
                            profile.double_opt_in ?
                            <i className="fa fa-times fa-fw fa-color-alert"></i>
                                :
                                <i className="fa fa-check fa-fw fa-color-info"></i>


                        }
                    </span></td>
                </tr>
            )
        })
        return (
            <div className="m-t">
                <div className="ibox float-e-margins">
                    <div className="ibox-title bg-info">
                        <h5 className="font-bold text-uppercase "></h5>
                    </div>
                    <div className="ibox-content">
                        <table className="table table-hover">
                            <thead>

                            <tr>
                                <th className="text-center text-uppercase">Date</th>
                                <th className="text-center text-uppercase">Name</th>
                                <th className="text-center text-uppercase">Mobile</th>
                                <th className="text-center text-uppercase">Email</th>
                                <th className="text-center text-uppercase">Opt-In</th>
                            </tr>

                            </thead>
                            <tbody>
                            {tableNames}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

export default ConsumptionTable;
