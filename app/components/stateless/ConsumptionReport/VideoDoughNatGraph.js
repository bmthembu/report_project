import React from 'react';
import {Doughnut} from 'react-chartjs-2';
import {flatten} from 'lodash';

export default (props) => {
    function renderData(data) {
        let keys=[];
        data.map(function(v) {
            keys.push(Object.keys(v));
        });


        let values=[];
        data.map(function(v) {
            values.push(Object.values(v));
        });
        return {
            datasets: [{
                data: flatten(values),
                backgroundColor: [
                    '#64E572','#24CBE5','#36A2EB','#00a99d','#57e8ed','#DDDF00','#2f7ed8','#00a99d',
                    '#ffc220','#1279d1','#1a3253','#670000','#0f303a','#1c3a53','#7dc67b','#411350','#2d3751',
                ],
                hoverBackgroundColor: [
                    '#8bbc21','#00a99d','#2f7ed8','#DDDF00','#24CBE5','#64E572'
                ]
            }],
            labels: flatten(keys)
        }
    }

    return (
        <div className="ibox float-e-margins">
            <div className="ibox-title bg-info">

                    {
                        props.type === 'content' ?
                            <h5 className="font-bold text-uppercase m-b-n-xs">
                            Content Consumption | [ Video Title ]
                            </h5>
                        :
                            <h5 className="font-bold text-uppercase m-b-n-xs">
                                Video Consumption | [ Video Title ]
                            </h5>
                    }
            </div>
            <div className="ibox-content">
                <div className="bg-white">
                <Doughnut
                    data={renderData(props.data)}
                    height={85}
                    options={{
                        title: {
                            display: false,
                        },
                        legend: {
                            display: true,
                            position: "bottom"
                        }
                    }}
                />
            </div>
            </div>

        </div>

    )
}
