import React from 'react';
import { has } from 'lodash';

class DeviceTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let devices;
        if(has(this.props.data, 'top_ten')){
            devices = this.props.data.top_ten.map((device, index) => {
                return (
                    <tr className="font-bold" key={index}>
                        <td className="text-center text-capitalize">{device.visitors}</td>
                        <td></td>
                        <td></td>
                        <td className="text-center text-capitalize">{device.device_name}</td>
                        <td></td>
                        <td className="text-center text-capitalize">{device.form_factor}</td>
                        <td></td>
                    </tr>
                )
            })
        }

            return (
                <div className="col-lg-12 m-t-lg">
                    <div className="ibox float-e-margins">
                        <div className="ibox-title bg-info">
                            <h4 className="font-bold text-uppercase m-b-n-xs">Device List</h4>
                        </div>
                        <div className="ibox-content">
                            <table className="table table-hover table-responsive">
                                <thead>
                                <tr>
                                    <th className="text-uppercase text-center">Device Visits</th>
                                    <th></th>
                                    <th></th>
                                    <th className="text-uppercase text-center">Device</th>
                                    <th></th>
                                    <th className="text-uppercase text-center">From</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                {devices}

                                </tbody>

                            </table>
                        </div>
                    </div>

                </div>
            )
        }
}

export default DeviceTable;
