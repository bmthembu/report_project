import React from 'react';

class UserDetails extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        };
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    render() {
        let renderProfiles;
        if (this.props.userProfile.length > 0) {
            renderProfiles = this.props.userProfile.map((user, index) => {
                return (
                    <tr className="text-center" key={index}>
                        <td>{user.date}</td>
                        <td>{user.full_name}</td>
                        <td><a href={"tel:" + user.mobile} className="fa-color-new">{user.mobile}</a></td>
                        <td><a href={"mailto:" + user.email} className="fa-color-new">{user.email}</a></td>
                        <td className="text-center">
                            {
                                user.double_opt_in ?
                                    <i className="fa fa-check fa-fw fa-color-info"></i> :
                                    <i className="fa fa-times fa-fw fa-color-alert"></i>

                            }

                        </td>
                        <td className="text-center">{user.age}</td>
                        <td className="text-center">
                            {
                                user.gender === "Male" ?
                                    <i className="fa fa-mars fa-fw fa-color-notice"></i>
                                    :
                                    <i className="fa fa-venus fa-fw fa-color-purple"></i>
                            }
                        </td>
                        <td className="text-center">{user.face_time}</td>
                    </tr>
                )
            })
        }

        return (
            <div className="col-lg-12">
                <div className="ibox float-e-margins">
                    <div className="ibox-title bg-info">
                        <h5 className="font-bold text-uppercase m-b-n-xs">User Details</h5>
                        <div className="ibox-tools" onClick={this.toggle.bind(this)}>
                            <a className="collapsed collapse-link" role="button">
                                <i className="fa fa-chevron-up" style={{color: "#ffffff"}}></i>
                            </a>
                        </div>
                    </div>
                    <div className={"collapse" + (this.state.open ? ' in' : '')}>
                        <div className="ibox-content">
                            {
                                this.props.userProfile.length > 0 ?
                                    <table className="table table-hover">
                                        <thead>
                                        <tr>
                                            <th className="text-center text-uppercase">Date</th>
                                            <th className="text-center text-uppercase">Name &amp; Surname</th>
                                            <th className="text-center text-uppercase">Mobile</th>
                                            <th className="text-center text-uppercase">Email</th>
                                            <th className="text-center text-uppercase">Opt-In</th>
                                            <th className="text-center text-uppercase">Age</th>
                                            <th className="text-center text-uppercase">Gender</th>
                                            <th className="text-center text-uppercase">Facetime</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        {renderProfiles}
                                        </tbody>
                                    </table>
                                    :
                                    <div>
                                        <div className="sk-spinner sk-spinner-wordpress">
                                            <span className="sk-inner-circle"></span>
                                        </div>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserDetails;
