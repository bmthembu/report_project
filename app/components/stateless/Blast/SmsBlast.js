import React from 'react';
import {find, has} from 'lodash';


class SmsBlast extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        let blast;
        let sentChannel;
        let delivered;
        if(this.props.blastData.length > 0){
            blast = this.props.blastData.map((d, index) => {
                delivered = find(d.blast_stats, function (bStats) {
                    return bStats.status === "delivered";
                });
                sentChannel = find(d.blast_stats, function (bStats) {
                    return bStats.status === "sent";
                });
                return (
                    <div className="col-lg-12">
                        <div className="ibox  float-e-margins">
                                <table className="table table-hover table-responsive ibox-title bg-info">
                                    <thead>
                                    <tr>
                                        <th className="text-uppercase">Channel</th>
                                        <th className="text-uppercase">Sent</th>
                                        <th></th>
                                        <th></th>
                                        <th className="text-uppercase">Delivered</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr className="font-bold bg-white">
                                        <td>
                                            <i className="fa fa-comments-o fa-fw" style={{color: "#f7941d"}}></i>
                                            <span className="text-capitalize fa-color-notice">{d.channel} Blast {index + 1}</span>
                                        </td>
                                        <td>
                                            {
                                                has(sentChannel, 'count') ?
                                                <span className="fa-color-notice">{sentChannel.count}</span>
                                                    :
                                                    <span className="fa-color-notice">{d.total_blast}</span>
                                            }
                                        </td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            {

                                                has(delivered, 'count') ?
                                               <span className="fa-color-notice">{delivered.count}</span>
                                                    :
                                                    0
                                            }
                                            <span className="fa-color-alert">|</span>
                                            {
                                                has(delivered, 'count') ?
                                                    <span className="fa-color-info">{delivered.pct}%</span>
                                                    :
                                                    0
                                            }
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            <div className="triangle-border triangle-border-left">
                                <h4 className="text-capitalize">Message</h4>
                                <p>{d.message}
                                    <small className="pull-right text-muted-more">
                                        <i>Date Executed {d.dt_stamp}</i>
                                        <i className="fa fa-check fa-color-notice m-l-sm"></i>
                                        <i className="fa fa-check fa-color-notice"></i>
                                    </small>
                                </p>
                            </div>
                        </div>
                    </div>
                )
            })
        }

        return (
            <div className="m-t">
                <div className="col-lg-12 m-b-lg">
                    <div className="bg-info">
                        <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase font-bold">
                            Push Channels
                        </h3>
                    </div>

                </div>
                {
                    this.props.blastData.length > 0 ?
                        blast
                        :
                        <div className="text-capitalize font-bold text-center text-success">
                            <h1>Blast information is currently unavailable</h1>
                        </div>
                }

            </div>
        )
    }
}

export default SmsBlast;