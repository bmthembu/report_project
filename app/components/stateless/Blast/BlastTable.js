import React from 'react';
import {find, has} from 'lodash';


class BlastTable extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let blast;
        let sentChannel;
        let delivered;
        if (this.props.blastData.length > 0) {
            blast = this.props.blastData.map((d, index) => {
                delivered = find(d.blast_stats, function (bStats) {
                    return bStats.status === "delivered";
                });
                sentChannel = find(d.blast_stats, function (bStats) {
                    return bStats.status === "sent";
                });
                return (
                    <tr className="font-bold">
                        <td>
                            <i className="fa fa-comments-o fa-fw" style={{color: "#f7941d"}}></i>
                            <span className="text-capitalize ">{d.channel}</span> Blast {index + 1}
                        </td>
                        <td>
                            {
                                has(sentChannel, 'count') ?
                                sentChannel.count | <span className="fa-color-info">100 %</span>
                                    :
                                    0
                            }
                            |
                            {
                                has(sentChannel, 'count') ?
                                    <span className="fa-color-info">{sentChannel.pct}%</span>
                                    :
                                    0
                            }
                        </td>
                        <td></td>
                        <td></td>
                        <td>
                            {

                                has(delivered, 'count') ?
                                delivered.count | <span className="fa-color-info">100 %</span>
                                    :
                                    0
                            }
                            |
                            {
                                has(delivered, 'count') ?
                                    <span className="fa-color-info">{delivered.pct}%</span>
                                    :
                                    0
                            }
                        </td>
                    </tr>
                )
            })
        }

        return (
            <div className="col-lg-12">
                <div className="ibox float-e-margins">
                    <div className="ibox-title bg-info">
                        <h4 className="font-bold text-uppercase m-b-n-xs">SMS Overview</h4>
                    </div>
                    <div className="ibox-content">
                        {
                            this.props.blastData.length > 0 ?
                                <table className="table table-hover table-responsive">
                                    <thead>
                                    <tr>
                                        <th className="text-uppercase">Channel</th>
                                        <th className="text-uppercase">Sent</th>
                                        <th></th>
                                        <th></th>
                                        <th className="text-uppercase">Delivered</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    {blast}

                                    </tbody>

                                </table>
                                :
                                <div className="text-capitalize font-bold text-center text-success">
                                    <h1>SMS Overview Information is currently unavailable</h1>
                                </div>
                        }
                    </div>
                </div>
            </div>
        )
    }
}

export default BlastTable;
