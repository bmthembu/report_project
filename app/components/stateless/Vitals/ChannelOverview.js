import React from 'react';
import {getChannelSummary} from '../../../api';
import _ from 'lodash';

class ChannelOverview extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            channelPush: {},
            channelPull: {},
            open: true
        };
        this.getChannelsOverview();
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    getChannelsOverview() {
        getChannelSummary(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({
                    channelPush: _.filter(results, function (result) {
                        return result.channel_type == 'push'
                    })
                });
                this.setState({
                    channelPull: _.filter(results, function (result) {
                        return result.channel_type == 'pull'
                    })
                });
            })
    }

    render() {
        let pullChannelRows;
        let pushChannelRows;
        if (this.state.channelPull.length > 0) {
            pullChannelRows = this.state.channelPull.map(function (pullValue) {
                return (
                    <tr className="font-bold">
                        <td>
                            <div className="col-xs-9 no-padding text-center">
                                <span className="text-capitalize m-b-xs">{pullValue.channel}</span>
                            </div>
                        </td>
                        <td className="text-center">{pullValue.sent} </td>
                        <td className="text-center">{pullValue.delivered} | <span
                            className="fa-color-info">{pullValue.delivered_pct}%</span>
                        </td>
                        <td className="text-center">{pullValue.visitors}</td>
                        <td className="text-center">{pullValue.users} | <span
                            className="fa-color-info">{pullValue.users_pct}%</span>
                        </td>
                        <td className="text-center">{pullValue.face_time}</td>
                    </tr>
                )
            })

        }

        if (this.state.channelPush.length > 0) {
            pushChannelRows = this.state.channelPush.map(function (pushValue) {
                return (
                    <tr className="font-bold">
                        <td>
                            <div className="col-xs-9 no-padding text-center">
                                <span className="text-capitalize m-b-xs">{pushValue.channel}</span>
                            </div>
                        </td>
                        <td className="text-center">{pushValue.sent} </td>
                        <td className="text-center">{pushValue.delivered} | <span
                            className="fa-color-info">{pushValue.delivered_pct}%</span>
                        </td>
                        <td className="text-center">{pushValue.visitors} | <span
                            className="fa-color-info">{pushValue.visitors_blast_pct}%</span></td>
                        <td className="text-center">{pushValue.users} | <span
                            className="fa-color-info">{pushValue.users_pct}%</span></td>
                        <td className="text-center">{pushValue.face_time} </td>
                    </tr>
                )
            })

        }

        return (
            <div className="col-lg-12">
                <div className="ibox float-e-margins">
                    <div className="ibox-title bg-info">
                        <h5 className="font-bold text-uppercase m-b-n-xs">
                            Channel Overview
                        </h5>
                        <div className="ibox-tools" onClick={this.toggle.bind(this)}>
                            <a className="collapsed collapse-link" role="button">
                                <i className="fa fa-chevron-up" style={{color: "#ffffff"}}></i>
                            </a>
                        </div>
                    </div>
                    {
                        this.state.channelPull || this.state.channelPull ?
                            <div className={"collapse" + (this.state.open ? ' in' : '')}>
                                <div className="ibox-content">
                                    <table className="table table-hover">
                                        <thead>
                                        <tr>
                                            <th className="text-center text-uppercase">Channel &amp; Industry <br/>Conversion
                                                Rate
                                            </th>
                                            <th className="text-center text-uppercase"><br/>Sent</th>
                                            <th className="text-center text-uppercase"><br/>Delivered</th>
                                            <th className="text-center text-uppercase"><span
                                                className="fa-color-new text-uppercase"><br/>Visitors</span>
                                            </th>
                                            <th className="text-center"><span
                                                className="fa-color-notice text-uppercase"><br/>Users</span>
                                            </th>
                                            <th className="text-center text-uppercase"><br/>Facetime</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr className="font-bold">
                                            <td className="text-center">
                                                <h3 className="text-uppercase m-b-xs col-xs-9">Pull Channels</h3>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        { pullChannelRows }

                                        <tr className="font-bold">
                                            <td className="text-center">
                                                <h3 className="font-bold text-uppercase m-b-xs col-xs-9">Push
                                                    Channels</h3>
                                            </td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                        { pushChannelRows }

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            :
                            <div>
                                <div className="sk-spinner sk-spinner-wordpress">
                                    <span className="sk-inner-circle"></span>
                                </div>
                            </div>

                    }

                </div>
            </div>
        )
    }

}


export default ChannelOverview