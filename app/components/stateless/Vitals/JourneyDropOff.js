import React from 'react';
import {getJourneyDropOff} from '../../../api';
import {has} from 'lodash';

class JourneyDropOff extends React.Component {
    constructor() {
        super();
        this.state = {
            JourneyDropOffData: {},
            open: {}
        };
        this.getJourneyDropOffData();
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    getJourneyDropOffData() {
        getJourneyDropOff(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({JourneyDropOffData: results});
            })
    }

    render() {
        let journeys;
        if (has(this.state.JourneyDropOffData, 'nodes')) {
            const widgetId = this.state.JourneyDropOffData.widget_id;
            journeys = this.state.JourneyDropOffData.nodes.map((data, index) => {
                return (
                    <div key={index}>
                        <div className="bg-white p-sm">
                            <hr className="m-t-sm m-b-sm"/>
                        </div>
                        <div className="col-sm-12 no-padding bg-white">

                            <div className="bg-white col-sm-15">
                                <div className="bg-white">
                                    <h4 className="no-margins font-bold text-uppercase text-center">
                                        <i className="fa fa-info-circle fa-fw"></i> {data.name}<br/>
                                        <small>ID | <a href="#" className="fa-color-new">{widgetId}</a>
                                        </small>
                                    </h4>
                                </div>
                            </div>

                            <div className="bg-white col-sm-15">
                                <div className="bg-white">
                                    <h4 className="no-margins font-bold text-uppercase text-center">
                                        {data.arrivals}<br/>
                                        <small className="fa-color-new">Visitors</small>
                                    </h4>
                                </div>
                            </div>

                            <div className="bg-white col-sm-15">
                                <div className="bg-white">
                                    <h4 className="no-margins font-bold text-uppercase text-center">
                                        <i className="fa fa-picture-o fa-fw"></i> {data.destination}<br/>
                                        <small>ID | <a href="#" className="fa-color-new">{widgetId}</a>
                                        </small>
                                    </h4>
                                </div>
                            </div>

                            <div className="bg-white col-sm-15">
                                <div className="bg-white">
                                    <h4 className="no-margins font-bold text-uppercase text-center">
                                        {data.departures}<br/>
                                        <small className="fa-color-notice">Users</small>
                                    </h4>
                                </div>
                            </div>

                            <div className="bg-white col-sm-15">
                                <div className="bg-white">
                                    <h4 className="no-margins font-bold text-uppercase text-center">
                                        {data.arrivals - data.departures} | <span className="fa-color-info">{data.drop_off_pct}%</span><br/>
                                        <small className="fa-color-alert">Drop-Off</small>
                                    </h4>

                                </div>
                            </div>
                        </div>
                        <div className="bg-white p-sm">
                            <hr className="m-t-sm m-b-sm"/>
                        </div>
                    </div>

                )
            })
        }
        return (
            <div>
                <div className="col-sm-12 m-t-lg">
                    <div className="bg-info">
                        <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                            Journey Drop-Off
                            <span className="pull-right">Channel | All Channels
                                <a className="collapsed collapse-link pull-right" onClick={this.toggle.bind(this)}
                                   role="button">
                                <i className="fa fa-chevron-up fa-color-white " style={{color: "#ffffff"}}></i>
                            </a>
                            </span>
                        </h3>
                    </div>
                    <div className={"collapse" + (this.state.open ? ' in' : '')}>
                    <div className="bg-white p-sm">
                        <div className="col-sm-15 no-padding">
                            <h3 className="font-bold text-uppercase no-margins m-b-xs text-center">Gate</h3>
                        </div>
                        <div className="col-sm-15 no-padding">
                            <h3 className="no-margins font-bold text-uppercase text-center">Arrivals</h3>
                        </div>
                        <div className="col-sm-15 no-padding">
                            <h3 className="no-margins font-bold text-uppercase text-center">Destinations</h3>
                        </div>
                        <div className="col-sm-15 no-padding">
                            <h3 className="no-margins font-bold text-uppercase text-center">Departures</h3>
                        </div>
                        <div className="col-sm-15 no-padding">
                            <h3 className="no-margins font-bold text-uppercase text-center">Drop-Off</h3>
                        </div>
                    </div>

                    <div className="bg-white p-sm">
                        <hr className="m-t-sm m-b-sm"/>
                    </div>


                    {
                        has(this.state.JourneyDropOffData, 'nodes') ?
                            <div>
                                <div id="5a0d4b75c2fd7969416f0652" className="col-sm-12 p-l-xl bg-white">
                                    <h3 className="no-margins font-bold text-uppercase">
                                        {this.state.JourneyDropOffData.widget_name} |
                                        <span className="fa-color-new">{this.state.JourneyDropOffData.widget_id}</span>
                                        <a href="http://v1.urup.com/interactions/5a0d4b75c2fd7969416f0652" target="_blank"
                                           className="fa-color-new pull-right p-r-xl">Preview Journey Link <i
                                            className="fa fa-external-link fa-fw"></i></a>
                                    </h3>
                                </div>
                                <br className="bg-white p-sm"></br>

                                { journeys }

                                <div className="bg-white p-sm">
                                    <hr className="m-t-sm m-b-sm"/>
                                </div>

                                <hr className="m-t-sm m-b-sm"/>
                                <hr className="m-t-sm m-b-sm"/>
                                <hr className="m-t-sm m-b-sm"/>
                                <hr className="m-t-sm m-b-sm"/>
                            </div>
                            :
                            <div className="text-capitalize font-bold text-center text-success">
                                <h1>Journey is currently unavailable</h1>
                            </div>
                    }
                    </div>
                    <div className="bg-white p-sm">
                        <hr className="m-t-sm m-b-sm"/>
                    </div>
                </div>
            </div>
        )
    }
}

export default JourneyDropOff;