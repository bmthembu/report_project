import React from 'react';
import { getVitalHeader } from '../../../api';

class VitalHeader extends React.Component {
    constructor(){
        super();
        this.state = {
            headerData: {}
        };
        this.getHeaderData();
    }
    getHeaderData(){
        getVitalHeader(window.localStorage.getItem('token'), window.localStorage.getItem('widgetId'))
            .then(results => {
                this.setState({headerData: results});
            })
    }


    render(){
        return (
            <div>
                <div className="row">
                    <div className="col-lg-3">
                        <div className="ibox float-e-margins">
                            <div className="ibox-title bg-info">
                                <h3 className="text-uppercase">
                                    Total <span className="fa-color-new"> Visitors </span>
                                </h3>
                            </div>
                            <div className="ibox-content">
                                <h1 className="no-margins">{this.state.headerData.total_visitors}</h1>
                                <hr className="m-t-sm m-b-sm"/>
                                <div className="stat-percent font-bold text-success">
                                    {this.state.headerData.total_visitors_pct}%
                                </div>
                                <median className="text-uppercase">Humans Arrived</median>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="ibox float-e-margins">
                            <div className="ibox-title bg-info">
                                <h3 className="text-uppercase">
                                    Total Users
                                </h3>
                            </div>
                            <div className="ibox-content">
                                <h1 className="no-margins">{this.state.headerData.total_users}</h1>
                                <hr className="m-t-sm m-b-sm"/>
                                <div className="stat-percent font-bold text-success">
                                    {this.state.headerData.total_users_pct}%
                                </div>
                                <median className="text-uppercase">Total Of Visitors</median>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="ibox float-e-margins">
                            <div className="ibox-title bg-info">
                                <h3 className="text-uppercase">
                                    Total <span className="fa-color-new"> Face-Time </span>
                                </h3>
                            </div>
                            <div className="ibox-content">
                                <h1 className="no-margins">
                                    {this.state.headerData.total_face_time}
                                </h1>
                                <hr className="m-t-sm m-b-sm"/>
                                <median className="text-uppercase">Per User</median>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3">
                        <div className="ibox float-e-margins">
                            <div className="ibox-title bg-info">
                                <h3 className="text-uppercase">
                                    Average Facetime
                                </h3>
                            </div>
                            <div className="ibox-content">
                                <h1 className="no-margins">
                                    {this.state.headerData.avg_face_time}
                                </h1>
                                <hr className="m-t-sm m-b-sm"/>
                                <median className="text-uppercase">Per User</median>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default VitalHeader;
