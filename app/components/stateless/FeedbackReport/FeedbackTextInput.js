import React from 'react';
import {has} from 'lodash'
class FeedbackTextInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        };
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    render() {
        let profiles = [];
        this.props.profiles.map((profile) => {
            profile.texts.map((text) => {
                if (text.seq === this.props.textInput.seq) {
                    profile['response_text'] = text.response;
                    profiles.push(profile);
                }
            });
        });
        let tableForm = profiles.map((mapProfile, index) => {
            return (
                <tr key={index}>
                    <td>
                        {mapProfile.fullname}<br/>
                        <small><a href="mailto:brettf@urup.com"
                                  className="fa-color-new">{mapProfile.email}</a><br/> <a
                            href="tel:+27 83 872 2185" className="fa-color-new">{mapProfile.phone}</a>
                        </small>
                    </td>
                    <td>{mapProfile.response_text}</td>
                </tr>                                         )
        });
        return (
                <div className="col-sm-12 m-t-lg">
                    <div className="col-sm-12 no-padding bg-black">
                        <div className="col-sm-12 no-padding bg-info">
                            <h3 className="m-sm fa-color-white text-uppercase">Text Input
                                <a className="collapsed collapse-link pull-right" onClick={this.toggle.bind(this)}
                                   role="button">
                                    <i className="fa fa-chevron-up" style={{color: "#ffffff"}}></i>
                                </a>
                            </h3>
                        </div>
                        <div className="clearfix"></div>
                        <div className={"collapse" + (this.state.open ? ' in' : '')}>
                            <div className="ibox-content">
                                <table className="table table-hover">
                                    <thead>
                                    <tr>
                                        <th className="text-center text-uppercase"><span className="fa-color-notice">User</span>
                                            Details
                                        </th>
                                        <th className="text-center text-uppercase">{this.props.textInput.prompt}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    {tableForm}
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div className="bg-white p-sm">
                            <hr className="m-t-sm m-b-sm"/>
                        </div>
                    </div>
                </div>

        )
    }
}
export default FeedbackTextInput; 
