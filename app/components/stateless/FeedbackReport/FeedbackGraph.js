import React from 'react';
import {HorizontalBar} from 'react-chartjs-2';

export default (props) => {
    function getChartData(data) {
        var renderAnswerDataSet = data.answers.map((data) => {
            return data.answer;
        });

        var renderCountDataSet = data.answers.map((data) => {
            return data.count;
        });
        return {
            datasets: [{
                data: renderCountDataSet,
                backgroundColor: [
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#FF9655', '#FFF263', '#6AF9C4', '#8bbc21', '#00a99d', '#2f7ed8',
                    '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#FF9655', '#FFF263', '#6AF9C4', '#8bbc21', '#00a99d', '#2f7ed8',
                    '#DDDF00', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572',
                    '#8bbc21', '#00a99d', '#2f7ed8', '#DDDF00', '#24CBE5', '#64E572'
                ],
                hoverBackgroundColor: [
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                    '#64E572', '#24CBE5', '#36A2EB', '#00a99d', '#57e8ed', '#DDDF00', '#2f7ed8', '#00a99d',
                ]
            }],
            labels: renderAnswerDataSet,
        }
    }

    return (
        <div className="bg-white">
            <HorizontalBar
                height={70}
                data={getChartData(props.data)}
                options={{
                    title: {
                        display: false,
                    },
                    legend: {
                        display: false,
                        position: "top"
                    },
                    plugins: {
                        datalabels: {
                            display: true,
                            align: 'end',
                            anchor: 'end'
                        }
                    }
                }}
            />

        </div>
    )

}

