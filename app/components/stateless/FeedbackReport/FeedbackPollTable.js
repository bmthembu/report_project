import React from 'react';
import {has} from 'lodash';
import FeedbackGraph from './FeedbackGraph';

class FeedbackPollTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        };
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    render() {
        let sumOfCount = 0;
        let sumOfPerc = 0;
        let PollGraphTable = this.props.poll.answers.map((data, index) => {
            sumOfCount += data.count;
            sumOfPerc += data.pct;
            return (
                <tr key={index}>
                    <td className="text-uppercase">{data.answer}</td>
                    <td className="text-center">{data.count}</td>
                    <td className="text-center">{data.pct}%</td>
                </tr>
            )
        });

        return (
            <div className="col-lg-12">
                <div className="col-lg-12 bg-info">
                    <h3 className="p-xs m-t-none m-b-none fa-color-white text-uppercase overflow-ellipsis">
                        {this.props.poll.id} | {this.props.poll.prompt}
                        <a className="collapsed collapse-link pull-right" onClick={this.toggle.bind(this)}
                           role="button">
                            <i className="fa fa-chevron-up" style={{color: "#ffffff"}}></i>
                        </a>
                    </h3>
                </div>
                <div className={"collapse" + (this.state.open ? ' in' : '')}>
                    <FeedbackGraph data={this.props.poll}/>

                    <div className="col-sm-12 noPadding bg-white">
                        <div className="clearfix"></div>
                        <div className="bg-white m-t-lg m-b">
                            <table className="table m-b-none" style={{"table-layout": "fixed"}}>
                                <tbody>
                                { PollGraphTable }
                                <tr>
                                    <td className="text-uppercase font-bold">Total</td>
                                    <td className="text-center font-bold">{sumOfCount}%</td>
                                    <td className="text-center font-bold">{sumOfPerc}%</td>
                                </tr>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default FeedbackPollTable; 
