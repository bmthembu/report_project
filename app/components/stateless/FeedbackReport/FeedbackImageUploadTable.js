import React from 'react';

class FeedbackImageUploadTable extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        };
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    render() {
        let profiles = [];
        this.props.profiles.map((profile) => {
            profile.uploads.map((text) => {
                if (text.seq === this.props.imageUpload.seq) {
                    profiles.push(profile);
                }
            });
        });
        let tableForm = profiles.map((mapProfile, index) => {
            return (
                <tr key={index}>
                    <td>{mapProfile.date}</td>
                    <td>{mapProfile.fullname}</td>
                    <td><a href="tel:+27 83 872 2185" className="fa-color-new">{mapProfile.phone}</a></td>
                    <td><a href="mailto:brettf@urup.com" className="fa-color-new">{mapProfile.email}</a></td>
                    <td className="text-center"><i className="fa fa-check fa-fw fa-color-info"></i><span
                        style={{display: "none"}}>true</span></td>
                    <td className="text-center">{mapProfile.age}</td>
                    <td className="text-center"><i className="fa fa-mars fa-fw fa-color-notice"></i><span
                        style={{display: "none"}}>male</span></td>
                    <td className="text-center">
                        {
                            mapProfile.uploads[0].upload_urls[0] ?
                                <a
                                    href={mapProfile.uploads[0].upload_urls[0]}
                                    target="_blank" className="fa-color-new">
                                    View<i className="fa fa-external-link fa-fw"></i>
                                </a>
                                :
                                <span>No Image</span>
                        }
                    </td>
                </tr>
            )
        });
        return (
            <div>
                <br/>
                <div className="col-lg-12 m-t">
                    <div className="ibox float-e-margins">
                        <div className="ibox-title bg-info">
                            <h5 className="font-bold text-uppercase m-b-n-xs">Image Uploads</h5>
                            <div className="ibox-tools" onClick={this.toggle.bind(this)}>
                                <a className="collapsed collapse-link" role="button">
                                    <i className="fa fa-chevron-up" style={{color: "#ffffff"}}></i>
                                </a>
                            </div>
                        </div>
                        <div className={"collapse" + (this.state.open ? ' in' : '')}>
                            <div className="ibox-content">
                                <table className="table table-hover">
                                    <thead>

                                    <tr>
                                        <th className="text-center text-uppercase">Date</th>
                                        <th className="text-center text-uppercase">Name</th>
                                        <th className="text-center text-uppercase">Mobile</th>
                                        <th className="text-center text-uppercase">Email</th>
                                        <th className="text-center text-uppercase">Opt-In</th>
                                        <th className="text-center text-uppercase">Age</th>
                                        <th className="text-center text-uppercase">Gender</th>
                                        <th className="text-center text-uppercase">Image</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    {tableForm}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FeedbackImageUploadTable;

