import React from 'react';
import FeedbackPoll from './FeedbackPollTable';
import {has, find} from 'lodash';

class FeedbackPollGraphTableAssembler extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: true
        };
    }

    toggle() {
        this.setState({
            open: !this.state.open
        });
    }

    render() {
        let profiles = [];
        this.props.profiles.filter((data) => {
            if (has(data.sequences, this.props.poll.seq)) {
                profiles.push(data);
            }
        });

        let table;
        if (profiles) {
            table = profiles.map((profile, index) => {
                return (
                    <tr key={index}>
                        <td>{profile.date}</td>
                        <td>{profile.fullname}</td>
                        <td><a href="tel:+27 83 872 2185" className="fa-color-new">{profile.phone}</a></td>
                        <td><a href="mailto:brettf@urup.com" className="fa-color-new">{profile.email}</a></td>
                        <td><i className="fa fa-check fa-fw fa-color-info"></i><span style={{display: "none"}}>
                            {
                                profile.opt_in ?
                                    <i className="fa fa-check fa-fw fa-color-info"></i> :
                                    <i className="fa fa-times fa-fw fa-color-alert"></i>

                            }
                        </span></td>
                        <td>{profile.age}</td>
                        <td><i className="fa fa-mars fa-fw fa-color-notice"></i><span style={{display: "none"}}>
                            {
                                profile.gender === "Male" ?
                                    <i className="fa fa-mars fa-fw fa-color-notice"></i>
                                    :
                                    <i className="fa fa-venus fa-fw fa-color-purple"></i>

                            }
                        </span></td>
                        <td>
                            {
                                profile.polls.map((poll) => {
                                    if (poll.seq === this.props.poll.seq) {
                                        return poll.response
                                    }
                                })
                            }
                        </td>
                    </tr>
                )
            })
        }
        return (
            <div>
                <FeedbackPoll poll={this.props.poll}/>
                <div className="col-lg-12 m-t">
                    <div className="ibox float-e-margins">
                        <div className="ibox-title bg-info">
                            <h5 className="font-bold text-uppercase m-b-n-xs">{this.props.poll.id}
                                | {this.props.poll.prompt}</h5>
                            <div className="ibox-tools" onClick={this.toggle.bind(this)}>
                                <a className="collapsed collapse-link" role="button">
                                    <i className="fa fa-chevron-up" style={{color: "#ffffff"}}></i>
                                </a>
                            </div>
                        </div>
                        <div className={"collapse" + (this.state.open ? ' in' : '')}>
                            <div className="ibox-content">
                                <table className="table table-hover">
                                    <thead>

                                    <tr>
                                        <th className="text-uppercase">Date</th>
                                        <th className="text-uppercase">Name</th>
                                        <th className="text-uppercase">Mobile</th>
                                        <th className="text-uppercase">Email</th>
                                        <th className="text-uppercase">Opt-In</th>
                                        <th className="text-uppercase">Age</th>
                                        <th className="text-uppercase">Gender</th>
                                        <th className="text-uppercase">Result</th>
                                    </tr>

                                    </thead>
                                    <tbody>
                                    {table}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default FeedbackPollGraphTableAssembler;

