import React from 'react'
import Main from '../components/layouts/Main';
import Blank from '../components/layouts/Blank';

import VitalView from '../views/Vital';
import VitalViewRoute from '../views/VitalRouter';
import BlastView from '../views/Blast';
import UserProfile from '../views/UserProfile';
import FeedbackReport from '../views/FeedbackReport';
import DeviceReport from '../views/DeviceReport';
import ConsumptionReport from '../views/ConsumptionReport';

import { Route, Router, IndexRedirect, browserHistory} from 'react-router';

export default (
    <Router history={browserHistory}>
      <Route path="/" component={Main}>
        <IndexRedirect to="/vital" />
          <Route path="vital" component={VitalViewRoute}> </Route>
        <Route path="vital/:widgetId/:token" component={VitalView}> </Route>
        <Route path="blast" component={BlastView}> </Route>
        <Route path="userProfile" component={UserProfile}> </Route>
        <Route path="feedback" component={FeedbackReport}> </Route>
        <Route path="consumption" component={ConsumptionReport}> </Route>
        <Route path="device" component={DeviceReport}> </Route>
      </Route>
    </Router>

);
